
############# ADOLC LIBRARIES #####################
ADOLC_PREFIX = /usr/local
ADOLC_LIB_DIR = $(ADOLC_PREFIX)/lib/x86_64-linux-gnu
ADOLC_INC_DIR = $(ADOLC_PREFIX)/include/adolc
ADOLC_LIBS    = -L$(ADOLC_LIB_DIR) -ladolc -Wl,-rpath,$(ADOLC_LIB_DIR) -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -L$(ADOLC_LIB_DIR) -lboost_system -lColPack -lm
ADOLC_CFLAGS    = -I$(ADOLC_INC_DIR)
############# ADOLC LIBRARIES #####################



############# EIGEN3 LIBRARIES #####################
EIGEN3_PREFIX = /usr/local
EIGEN3_INC_DIR = $(EIGEN3_PREFIX)/include/eigen3
EIGEN3_CFLAGS    = -I$(EIGEN3_INC_DIR)
############# EIGEN3 LIBRARIES #####################



############# IPOPT LIBRARIES #####################
IPOPT_PREFIX = /usr/local
IPOPT_LIB_DIR = $(IPOPT_PREFIX)/lib
IPOPT_INC_DIR = $(IPOPT_PREFIX)/include/coin
IPOPT_LIBS    = -L$(IPOPT_LIB_DIR) -lipopt -lcoinmumps -lblas  -L/usr/lib/gcc/x86_64-linux-gnu/9 -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/9/../../.. -llapack -lblas -lcoinmumps -ldl -lgfortran -lm -lquadmath -lblas -lm  -ldl
IPOPT_CFLAGS    = -I$(IPOPT_INC_DIR) -DHAVE_CSTDDEF
############# IPOPT LIBRARIES #####################



############# PSOPT LIBRARIES #####################
PSOPT_PREFIX = /usr/local
PSOPT_LIB_DIR = $(PSOPT_PREFIX)/lib/PSOPT
PSOPT_INC_DIR = $(PSOPT_PREFIX)/include/PSOPT
PSOPT_LIBS    = $(PSOPT_LIB_DIR)/libPSOPT.a
PSOPT_CFLAGS    = -I$(PSOPT_INC_DIR)
############# PSOPT LIBRARIES #####################



CFLAGS = $(PSOPT_CFLAGS) $(ADOLC_CFLAGS) $(EIGEN3_CFLAGS) $(IPOPT_CFLAGS) 
ALL_LIBS = $(PSOPT_LIBS) $(ADOLC_LIBS) $(IPOPT_LIBS)



irep2: 
	$(CXX) -std=c++17 -g -o irep2 irep2.cxx $(CFLAGS) $(ALL_LIBS) 
	rm -f *.o
