    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// Expresions depending on one state //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #define PI 3.141592653589793

    // adouble gauss (adouble scale, adouble t, adouble t0, adouble sig)
    // {
    //   return scale*exp(-0.5*pow((t-t0)/sig,2));
    // }

    // adouble step(adouble t, adouble t0, adouble sig, adouble pt, adouble threshold){
    //     // if (sqrt(pt*pt) > threshold)
    //     // {
    //     //   return 0;
    //     // }
    //     // if (sqrt(pt * pt) <= threshold)
    //     // {
    //     //   return gauss(1, t, t0, sig / 4.) * pow(pt - threshold, 2.);
    //     // }

    //     return gauss(1, t, t0, sig) * pow(pt - threshold, 2.) * smooth_heaviside(threshold - pt, 0.1);
    // }

    // adouble mod_2(adouble a, adouble b)
    // {
    //     return pow(a, 2) + pow(b, 2);
    // }

    
    // adouble mod(adouble a, adouble b)
    // {
    //     return sqrt(mod_2(a, b));
    // }
        
  
    template<class type1, class type2>
    void H_dot_a(type1 *b, type2 **H, type1 *a, size_t Emax)
    {
        for (size_t i=0; i<Emax; i++)
        {
            b[i]=0;
            b[i+Emax]=0;
            for (size_t j=0; j<Emax; j++)
            {
                b[i]=b[i]+H[i][j]*a[j+Emax];
                b[i+Emax]=b[i+Emax]-H[i][j]*a[j];
            }
        }

    }
      
      
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// Expresions depending on two states //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    
    template<class type1, class type2>
    double cross_correl_2(type1 a, type2  b)
    {
      size_t nstate_a=a.size();
      size_t nstate_b=b.size();
      if (nstate_a != nstate_b)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: both states vectors must have the same dimension");

      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");

      size_t Emax= nstate_a/2;

      double re=0;
      double im=0; 
      for (size_t i=0; i<Emax; i++)
      {
          re = re + a(i)*b(i) - a(i+Emax)*b(i+Emax);
          im = im + a(i)*b(i+Emax) + a(i+Emax)*b(i);
      }

      return pow(re,2.)+pow(im,2.);
    }


    template< class type2>
    adouble cross_correl_2(type2 a,adouble *  b)
    {
      size_t nstate_a=a.size();
      
      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");

      size_t Emax= nstate_a/2;

      adouble re=0;
      adouble im=0; 
      for (size_t i=0; i<Emax; i++)
      {
          re = re + a(i)*b[i] - a(i+Emax)*b[i+Emax];
          im = im + a(i)*b[i+Emax] + a(i+Emax)*b[i];
      }

      return pow(re,2.)+pow(im,2.);
    }



    adouble cross_correl_2(adouble * a,adouble *  b, size_t nstates)
    {

      if (nstates % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");

      size_t Emax= nstates/2;

      adouble re=0;
      adouble im=0; 
      for (size_t i=0; i<Emax; i++)
      {
          re = re + a[i]*b[i] - a[i+Emax]*b[i+Emax];
          im = im + a[i]*b[i+Emax] + a[i+Emax]*b[i];
      }

      return pow(re,2.)+pow(im,2.);
    }



    template< class type1, class type2>
    adouble cross_correl_psi_int_2(type1 a, adouble *  b, type2 E, adouble t)
    {
      size_t nstate_a=a.size();
      
      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_psi_int_2: nstates_a must be a multiple of 2");

      size_t Emax= nstate_a/2;

      adouble re=0;
      adouble im=0; 
      double wn;
      for (size_t n=0; n<Emax; n++)
      {
          wn=E(n,n);
          re = re + (a(n)*b[n] + a(n+Emax)*b[n+Emax])*cos(wn*t) + (a(n)*b[n+Emax] - a(n+Emax)*b[n])*sin(wn*t);
          im = im + (a(n)*b[n+Emax] - a(n+Emax)*b[n])*cos(wn*t) - (a(n)*b[n] + a(n+Emax)*b[n+Emax] )*sin(wn*t);
      }

      return pow(re,2.)+pow(im,2.);
    }




    template<class type1>
    double norm(type1 a)
    {
       return pow(cross_correl_2(a,a),1/4.);
    }

    template<class type1>
    void normalize(type1 &a)
    {
      size_t nstate_a=a.size();
      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");
      
      size_t Emax= nstate_a/2;

      double norm_a=norm(a);
      for (size_t i=0; i<2*Emax; i++)
      {
        a(i)=a(i)/norm_a;
      }
    }


    adouble sigmoid (adouble x, double y0, double y1, adouble x0, double sigma)
    {
       return y0+(y1-y0)/(1+exp(-(x-x0)/sigma));
    }

  


