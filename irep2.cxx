//////////////////////////////////////////////////////////////////////////
//////////////////            lts.cxx        /////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////           PSOPT  Example             ////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////// Title: Linear tangent steering problem           ////////////////
//////// Last modified:         16 February 2009          ////////////////
//////// Reference:             Betts (2001)              ////////////////
//////// (See PSOPT handbook for full reference)          ////////////////
//////////////////////////////////////////////////////////////////////////
////////     Copyright (c) Victor M. Becerra, 2009        ////////////////
//////////////////////////////////////////////////////////////////////////
//////// This is part of the PSOPT software library, which ///////////////
//////// is distributed under the terms of the GNU Lesser ////////////////
//////// General Public License (LGPL)                    ////////////////
//////////////////////////////////////////////////////////////////////////


#include "psopt.h"
#include "linear_systems.h"
#include "gen_funct.h"



//////////////////////////////////////////////////////////////////////////
///////////////////  Define the end point (Mayer) cost function //////////
//////////////////////////////////////////////////////////////////////////


// int index1(int i, int j, int ny)
// {
//     return i*ny+j;
// }

// vector<int> index2(int k, int ny)
// {
//     vector<int> ij(2);
//     int i=k/ny;
//     int j=k%ny;

//     ij[0]=i;
//     ij[1]=j;

//     return ij;
// }


MatrixXd population(MatrixXd a)
{
    size_t n_states=a.rows();
    size_t n_nodes=a.cols();

    size_t Emax=n_states/2;


    MatrixXd pop=zeros(Emax,n_nodes);
    for (size_t i = 0; i<Emax; i++)
    {
        for (size_t j=0; j<n_nodes; j++)
        {
            pop(i,j)= pow( a(i,j),2) + pow(a(i+Emax,j),2) ;
        }
    }

    return pop;
}


// DMatrix ho_basis(vector<vector<double>> hn, DMatrix psi)
// {
//     size_t n_states=psi.getn();
//     size_t n_nodes=psi.getm();

//     size_t Emax=n_states/2;


//     DMatrix x_ho=zeros(2*Emax,n_nodes);
//     for (size_t t=0; t<n_nodes; t++)
//     {
//         for (size_t h=0; h<Emax; h++)
//         {
//             x_ho(h+1,t+1)=0; //Real part
//             x_ho(h+1+Emax,t+1)=0; //Imaginary part
//             for(size_t n=0; n<Emax; n++)
//             {
//                 x_ho(h+1,t+1)=x_ho(h+1,t+1) + hn[h][n]*psi(n+1,t+1);
//                 x_ho(h+1+Emax,t+1)=x_ho(h+1+Emax,t+1) + hn[h][n]*psi(n+1+Emax,t+1);
//             }
            

//         }
//     }

//     return x_ho;
// }



MatrixXd ho_basis_psi_int(MatrixXd hn, MatrixXd psi, MatrixXd E, MatrixXd time)
{
    size_t n_states=psi.rows();
    size_t n_nodes=psi.cols();

    size_t Emax=n_states/2;
    size_t n_ho=hn.rows();

    double wn;

    MatrixXd x_ho=MatrixXd::Zero(2*n_ho,n_nodes);
    
    for (size_t t=0; t<n_nodes; t++)
    {
        for (size_t h=0; h<n_ho; h++)
        {
            x_ho(h,t)=0; //Real part
            x_ho(h+n_ho,t)=0; //Imaginary part
            for(size_t n=0; n<Emax; n++)
            {
                wn=E(n,n);
                x_ho(h,t)=x_ho(h,t) + hn(h,n)*( psi(n,t)*cos(wn*time(t)) +  psi(n+Emax,t)*sin(wn*time(t)) );
                x_ho(h+n_ho,t)=x_ho(h+n_ho,t) + hn(h,n)*( -psi(n,t)*sin(wn*time(t)) +  psi(n+Emax,t)*cos(wn*time(t)) );
            }
            

        }
    }

    return x_ho;
}


MatrixXd psii_2_psis(MatrixXd psi, MatrixXd E, MatrixXd time)
{

    size_t Emax=psi.rows()/2;

    return ho_basis_psi_int(MatrixXd::Identity(Emax,Emax), psi, E, time);

}



// DMatrix x_expectation(DMatrix a, vector<vector<double>> mu)
// {
//     size_t n_states=a.getn();
//     size_t n_nodes=a.getm();

//     size_t Emax=n_states/2;


//     DMatrix x=zeros(1,n_nodes);
//     for (size_t j=1; j<=n_nodes; j++)
//     {
//         x(1,j)= 0;
//         for (size_t n=1; n<=Emax; n++)
//         {
//             for (size_t m=1; m<=Emax; m++)
//             {
//                 x(1,j)=x(1,j) + ( a(n,j)*a(m,j) + a(n+Emax,j)*a(m+Emax,j) )*mu[n-1][m-1];
//             }
//         }
        
//     }

//     return x;
// }

MatrixXd join_diagonal_matrix(MatrixXd mat1, MatrixXd mat2)
{
    size_t n1=mat1.rows();
    size_t n2=mat2.rows();
    size_t n=n1+n2;

    MatrixXd mat_final(n, n);

    for (int i=0; i<n1; i++)
    {
        mat_final(i,i)=mat1(i,i);
    }
    for (int i=0; i<n2; i++)
    {
        mat_final(i+n1,i+n1)=mat2(i,i);
    }

    return mat_final;
}



class System
{
public:
    MatrixXd mu11;  //Dipole matrix
    MatrixXd mu12;  //Dipole matrix
    MatrixXd mu22;  //Dipole matrix
    
    MatrixXd Q11;  //Coupling terms
    MatrixXd Q12;  //Coupling terms
    MatrixXd Q22;  //Coupling terms

    MatrixXd P12;  //Coupling terms

    

    MatrixXd E1;   //Diagonal version of eigen energies
    MatrixXd E2;   //Diagonal version of eigen energies
    MatrixXd E;   //Diagonal version of eigen energies

    MatrixXd T11;  //Decay coeficients matrix
    double kappa;

    size_t Emax1;
    size_t Emax2;
    size_t Emax;


    //Constructors
    System(){};
    System(string E1_file, string E2_file, string mu11_file, string mu12_file, string mu22_file, string Q11_file, string Q12_file, string Q22_file, string P12_file, string T11_file, double kappa, size_t Emax1, size_t Emax2) : Emax1(Emax1), Emax2(Emax2), kappa(kappa)
    {
        //E_vec = readMatrix_eigen(E_file).rows(0);
        //E=vec2diag(E_vec);

        Emax=Emax1+Emax2;

        E1= readMatrix_eigen(E1_file).block(0,0,Emax1,Emax1);
        E2= readMatrix_eigen(E2_file).block(0,0,Emax2,Emax2);
        E=join_diagonal_matrix(E1,E2);
        mu11= readMatrix_eigen(mu11_file).block(0,0,Emax1,Emax1);
        mu12= readMatrix_eigen(mu12_file).block(0,0,Emax1,Emax2);
        mu22= readMatrix_eigen(mu22_file).block(0,0,Emax2,Emax2);
        Q11= readMatrix_eigen(Q11_file).block(0,0,Emax1,Emax1);
        Q12= readMatrix_eigen(Q12_file).block(0,0,Emax1,Emax2);
        Q22= readMatrix_eigen(Q22_file).block(0,0,Emax2,Emax2);
        P12= readMatrix_eigen(P12_file).block(0,0,Emax1,Emax2);
        T11= readMatrix_eigen(T11_file).block(0,0,Emax1,Emax1);


    };
    //Destructors
    ~System(){}; // This is the destructor declaration

private:
    // VectorXd E_vec; //Vector version of eigenenergies
    // string E1_file;
    // string E2_file;
    // string mu11_file;
    // string mu12_file;
    // string mu22_file;
    // string P12_file;
    // string Q12_file;

};




class User_Param
{
    public:

        //Initial and final state information
        size_t Emax1;
        size_t Emax2;
        size_t Emax;

        size_t nstates;
        VectorXd init_state;
        VectorXd final_state;

        MatrixXd rest_state;
        size_t n_rest_states;
        VectorXd k_rest;


        //Bounds
        double u_l;
        double u_u;
        double tf_l;
        double tf_u;  

        //Algorithm parameters
        int nnodes;
        string init_cond;
        string ipopt_linear_solver;
        string collocation_method;
        size_t nlp_iter_max;
        double nlp_tolerance;
        string hessian;
        string inter_schrod;

        System sys;

        //Cost parameters
        double int_costsc;
        double end_cost;
        double init_cost;
        double sin_err;
        double sin_exp;
        double ipopt_max_cpu_time;
        double glob_k;
        double tf_k;
        double w_rest;
        double w_rest_disp;

        //System parameters
        double kappa;
        

        

        User_Param()
        {
            string aux;
            //Initial and final state information

            cout << "Enter Emax1: ";
            cin >> Emax1;
            cin >> aux;
            cout << Emax1 << endl;

            cout << "Enter Emax2: ";
            cin >> Emax2;
            cin >> aux;
            cout << Emax2 << endl;

            Emax=Emax1+Emax2;

            nstates=2*Emax;



            cout << "Enter initial state file: ";
            cin >> init_file;
            cin >> aux;
            cout << init_file << endl;
            init_state=readMatrix_eigen(init_file).row(0);

            cout<<"Read vector:";
            print_vector(init_state);

            if (init_state.size() < nstates)
                throw logic_error("*********** ERROR *************   User_Param: the dimension of init_state must greater than 2*Emax");
            normalize(init_state);

            init_state.resize(nstates);
            cout<<"After resize and normalization:";
            print_vector(init_state);

            


            cout << "Enter final state file: ";
            cin >> final_file;
            cin >> aux;
            cout << final_file << endl;
            final_state=readMatrix_eigen(final_file).row(0);

            cout<<"Read vector:";
            print_vector(final_state);

            if (final_state.size() < nstates)
                throw logic_error("*********** ERROR *************   User_Param: the dimension of final_state must greater than 2*Emax");
            normalize(final_state);

            final_state.resize(nstates);

            cout<<"After recize and normalization:";
            print_vector(final_state);
            

            //Bounds
            cout << "Enter u max: ";
            cin >> u_u;
            cin >> aux;
            cout << u_u << endl;

            cout << "Enter u min: ";
            cin >> u_l;
            cin >> aux;
            cout << u_l << endl;

            cout << "Enter final time max: ";
            cin >> tf_u;
            cin >> aux;
            cout << tf_u << endl;

            cout << "Enter final time min: ";
            cin >> tf_l;
            cin >> aux;
            cout << tf_l << endl;

            //Algorithm parameters
            cout << "Interaction or Schrodinger representation: ";
            cin >> inter_schrod;
            cin >> aux;
            cout << inter_schrod << endl;

            cout << "Enter number of nodes: ";
            cin >> nnodes;
            cin >> aux;
            cout << nnodes << endl;

            cout << "Enter init_cond (random/linear): ";
            cin >> init_cond;
            cin >> aux;
            cout << init_cond << endl;
           
            cout << "Enter Ipopt linear solver (ma27/ma57/ma77/ma86/ma97/pardiso/wsmp/mumps/spral): ";
            cin >> ipopt_linear_solver;
            cin >> aux;
            cout << ipopt_linear_solver << endl;

            cout << "Enter Collocation method (Legendre/Chebyshev/trapezoidal/Hermite-Simpson): ";
            cin >> collocation_method;
            cin >> aux;
            cout << collocation_method << endl;

            cout << "Enter nlp_iter_max: ";
            cin >> nlp_iter_max;
            cin >> aux;
            cout << nlp_iter_max << endl;

            cout << "Enter nlp_tolerance: ";
            cin >> nlp_tolerance;
            cin >> aux;
            cout << nlp_tolerance << endl;

            cout << "Enter hessian (limited-memory/exact): ";
            cin >> hessian;
            cin >> aux;
            cout << hessian << endl;

            //System class
            cout << "Enter Eigenvalue file 1: ";
            cin >> E1_file;
            cin >> aux;
            cout << E1_file << endl;

            cout << "Enter Eigenvalue file 2: ";
            cin >> E2_file;
            cin >> aux;
            cout << E2_file << endl;

            cout << "Enter Dipole matrix file mu11: ";
            cin >> mu11_file;
            cin >> aux;
            cout << mu11_file << endl;

            cout << "Enter Dipole matrix file mu12: ";
            cin >> mu12_file;
            cin >> aux;
            cout << mu12_file << endl;

            cout << "Enter Dipole matrix file mu22: ";
            cin >> mu22_file;
            cin >> aux;
            cout << mu22_file << endl;

            cout << "Enter coupling file Q11: ";
            cin >> Q11_file;
            cin >> aux;
            cout << Q11_file << endl;

            cout << "Enter coupling file Q12: ";
            cin >> Q12_file;
            cin >> aux;
            cout << Q12_file << endl;

            cout << "Enter coupling file Q22: ";
            cin >> Q22_file;
            cin >> aux;
            cout << Q22_file << endl;


            cout << "Enter coupling file P12: ";
            cin >> P12_file;
            cin >> aux;
            cout << P12_file << endl;


            cout << "Enter coupling file T11: ";
            cin >> T11_file;
            cin >> aux;
            cout << T11_file << endl;


            cout << "Enter kappa: ";
            cin >> kappa;
            cin >> aux;
            cout << kappa << endl;

            

            

            sys=System(E1_file, E2_file, mu11_file, mu12_file, mu22_file, Q11_file, Q12_file, Q22_file, P12_file, T11_file, kappa, Emax1, Emax2);


            //Cost parameters
            cout << "Enter integral_cost_scale: ";
            cin >> int_costsc;
            cin >> aux;
            cout << int_costsc << endl;

            cout << "Enter init_cost: ";
            cin >> init_cost;
            cin >> aux;
            cout << init_cost << endl;

            cout << "Enter end_cost: ";
            cin >> end_cost;
            cin >> aux;
            cout << end_cost << endl;

            cout << "Enter sin_error: ";
            cin >> sin_err;
            cin >> aux;
            cout << sin_err << endl;

            cout << "Enter sin_exp: ";
            cin >> sin_exp;
            cin >> aux;
            cout << sin_exp << endl;

            cout << "Enter ipopt_max_cpu_time: ";
            cin >> ipopt_max_cpu_time;
            cin >> aux;
            cout << ipopt_max_cpu_time << endl;


            //Cost parameters
            cout << "Enter glob_k: ";
            cin >> glob_k;
            cin >> aux;
            cout << glob_k << endl;

            //Cost parameters
            cout << "Enter tf_k: ";
            cin >> tf_k;
            cin >> aux;
            cout << tf_k << endl;


            cout << "Enter rest_state_file: ";
            cin >> rest_state_file;
            cin >> aux;
            cout << rest_state_file << endl;
            rest_state=readMatrix_eigen(rest_state_file);


             if (rest_state.rows() != 0)
                if (rest_state.cols() != nstates)
                    throw logic_error("*********** ERROR *************   User_Param: the dimension of rest_state must be equal to 2*Emax");

            


            cout << "Enter k_rest_file: ";
            cin >> k_rest_file;
            cin >> aux;
            cout << k_rest_file << endl;
            k_rest=readMatrix_eigen(k_rest_file).col(0);
            if ( rest_state.rows() != k_rest.rows())
                throw logic_error("*********** ERROR *************   User_Param: k_rest and rest_state has to be the same size");
            

            //Restricted omega
            cout << "Enter w_rest: ";
            cin >> w_rest;
            cin >> aux;
            cout << w_rest << endl;

            //Restricted omega
            cout << "Enter w_rest_disp: ";
            cin >> w_rest_disp;
            cin >> aux;
            cout << w_rest_disp << endl;
        };

    private:
        string inp_file;
        string init_file;
        string final_file;
        string rest_state_file;
        string k_rest_file;


        string E1_file;
        string E2_file;
        string mu11_file;
        string mu12_file;
        string mu22_file;
        
        string Q11_file;
        string Q12_file;
        string Q22_file;

        string P12_file;

        string T11_file;

        

};


adouble endpoint_cost(adouble* initial_states, adouble* final_states,
                      adouble* parameters,adouble& t0, adouble& tf,
                      adouble* xad, int iphase, Workspace* workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;

    adouble ret;

    if (uparam->inter_schrod == "Interaction")
    {
        ret= -uparam->end_cost*cross_correl_psi_int_2(uparam->final_state,final_states,uparam->sys.E,tf) - uparam->init_cost*cross_correl_psi_int_2(uparam->init_state,initial_states,uparam->sys.E,0) + uparam->tf_k*pow(tf - 0.5*(uparam->tf_u + uparam->tf_l),2.);
    }
    else if (uparam->inter_schrod == "Schrodinger")
    {
        ret= -uparam->end_cost*cross_correl_psi_int_2(uparam->final_state,final_states,uparam->sys.E,0) - uparam->init_cost*cross_correl_psi_int_2(uparam->init_state,initial_states,uparam->sys.E,0) + uparam->tf_k*pow(tf - 0.5*(uparam->tf_u + uparam->tf_l),2.);
    }

    return ret;
}

// //////////////////////////////////////////////////////////////////////////
// ///////////////////  Define the integrand (Lagrange) cost function  //////
// //////////////////////////////////////////////////////////////////////////

adouble integrand_cost(adouble* states, adouble* controls,
                       adouble* parameters, adouble& time, adouble* xad,
                       int iphase, Workspace* workspace)
{
    adouble Et = controls[ CINDEX(1) ];

    User_Param *uparam = (User_Param *)workspace->problem->user_data;

    size_t n_rest_states=uparam->rest_state.rows();
    size_t Emax=uparam->Emax;
    double k;

    adouble tf=get_final_time( xad, 1, workspace);

    adouble state_error =0;
    for(size_t i=0; i<n_rest_states;i++)
    {
        k=uparam->k_rest(i);
        if (uparam->inter_schrod == "Interaction")
        {   
            if (i == 0)
                state_error = state_error + k*cross_correl_psi_int_2(uparam->rest_state.row(i),states,uparam->sys.E,time)*sigmoid(time, 0, 1, tf/3., 5);
            if (i == 1)
                state_error = state_error + k*cross_correl_psi_int_2(uparam->rest_state.row(i),states,uparam->sys.E,time)*sigmoid(time, 1, 0, 2*tf/3., 5);
            if (i == 2)
                state_error = state_error + k*cross_correl_psi_int_2(uparam->rest_state.row(i),states,uparam->sys.E,time)*sigmoid(time, 1, 0, tf/3., 5)*sigmoid(time, 0, 1, 2*tf/3., 5);

                
        }
        else if (uparam->inter_schrod == "Schrodinger")
        {
            state_error = state_error + k*cross_correl_psi_int_2(uparam->rest_state.row(i),states,uparam->sys.E,0);
        }
    }
    
    return uparam->int_costsc * Et * Et / (pow(sin(time * PI / tf), uparam->sin_exp) + uparam->sin_err) + uparam->glob_k*state_error;


}

//////////////////////////////////////////////////////////////////////////
///////////////////  Define the DAE's ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void dae(adouble* derivatives, adouble* path, adouble* states,
         adouble* controls, adouble* parameters, adouble& time,
         adouble* xad, int iphase, Workspace* workspace)
{

    User_Param *uparam = (User_Param *)workspace->problem->user_data;

    size_t Emax=uparam->Emax;
    size_t Emax1=uparam->Emax1;
    size_t Emax2=uparam->Emax2;

    adouble Et=controls[ CINDEX(1) ];
    
    double wij;

    double kappa=uparam->sys.kappa;

    if (uparam->inter_schrod == "Interaction")
    {
        for (size_t i=0; i<Emax; i++)
        {
            derivatives[i]=0;
            derivatives[i+Emax]=0;
            for (size_t j=i; j<Emax; j++)
            {
                wij=uparam->sys.E(i,i)-uparam->sys.E(j,j);

                if (i< Emax1 && j<Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu11(i,j)*Et + uparam->sys.Q11(i,j) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] )   +    ( - 0.5*kappa*uparam->sys.T11(i,j) ) * ( cos(wij*time)*states[j] - sin(wij*time)*states[j+Emax] )  ;
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu11(i,j)*Et + uparam->sys.Q11(i,j) ) * ( sin(wij*time)*states[j+Emax] - cos(wij*time)*states[j] )   +   ( - 0.5*kappa*uparam->sys.T11(i,j) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                }

                if (i< Emax1 && j>=Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu12(i,j-Emax1)*Et + uparam->sys.P12(i,j-Emax1) + uparam->sys.Q12(i,j-Emax1) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu12(i,j-Emax1)*Et + uparam->sys.P12(i,j-Emax1) + uparam->sys.Q12(i,j-Emax1) ) * ( - cos(wij*time)*states[j] + sin(wij*time)*states[j+Emax] );
                }

                if (i>= Emax1 && j>=Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu22(i-Emax1,j-Emax1)*Et + uparam->sys.Q22(i-Emax1,j-Emax1) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu22(i-Emax1,j-Emax1)*Et + uparam->sys.Q22(i-Emax1,j-Emax1) ) * ( - cos(wij*time)*states[j] + sin(wij*time)*states[j+Emax] );
                }   
            }


            /// This sumation ensures that the Hamiltonian matrix is hermitian. Indexes i and j has been swaped and conjugation is given by wij.
            for (size_t j=0; j<i; j++)
            {
                wij=uparam->sys.E(i,i)-uparam->sys.E(j,j);

                if (i< Emax1 && j<Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu11(j,i)*Et + uparam->sys.Q11(j,i) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] )  +  ( - 0.5*kappa*uparam->sys.T11(j,i) ) * ( cos(wij*time)*states[j] - sin(wij*time)*states[j+Emax] );
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu11(j,i)*Et + uparam->sys.Q11(j,i) ) * ( sin(wij*time)*states[j+Emax] - cos(wij*time)*states[j] )  +  ( - 0.5*kappa*uparam->sys.T11(j,i) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                }

                if (i>= Emax1 && j<Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu12(j,i-Emax1)*Et + uparam->sys.P12(j,i-Emax1) + uparam->sys.Q12(j,i-Emax1) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu12(j,i-Emax1)*Et + uparam->sys.P12(j,i-Emax1) + uparam->sys.Q12(j,i-Emax1) ) * ( - cos(wij*time)*states[j] + sin(wij*time)*states[j+Emax] );
                }

                if (i>= Emax1 && j>=Emax1)
                {
                    derivatives[i]=derivatives[i] + ( - uparam->sys.mu22(j-Emax1,i-Emax1)*Et + uparam->sys.Q22(j-Emax1,i-Emax1) ) * ( cos(wij*time)*states[j+Emax] + sin(wij*time)*states[j] );
                    derivatives[i+Emax]=derivatives[i+Emax] + ( - uparam->sys.mu22(j-Emax1,i-Emax1)*Et + uparam->sys.Q22(j-Emax1,i-Emax1) ) * ( - cos(wij*time)*states[j] + sin(wij*time)*states[j+Emax] );
                }   

               
                
            }
        }
    }

    // else if (uparam->inter_schrod == "Schrodinger")
    // {
    //     for (size_t i=0; i<Emax; i++)
    //     {
    //         derivatives[i]=0;
    //         derivatives[i+Emax]=0;
    //         for (size_t j=0; j<Emax; j++)
    //         {
    //             derivatives[i]=derivatives[i]           + (uparam->sys.E[i][j] - uparam->sys.mu[i][j]*Et) * states[j+Emax];
    //             derivatives[i+Emax]=derivatives[i+Emax] - (uparam->sys.E[i][j] - uparam->sys.mu[i][j]*Et) * states[j];
    //         }
    //     }
    // }
    
    else
    {
        throw invalid_argument("****** ERROR *******  dae: uparam.inter_schrod must be Interaction or Schrodinger");
    }

    

    



}

////////////////////////////////////////////////////////////////////////////
/////////////////// Define the integrand of the integral constraint ///////
////////////////////////////////////////////////////////////////////////////
adouble et(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    adouble Et = controls[CINDEX(1)];
    return Et;
}

adouble et_cos(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest;

    adouble Et_cos = controls[CINDEX(1)]*cos(w_rest*time);
    return Et_cos;
}


adouble et_sin(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest;

    adouble Et_sin = controls[CINDEX(1)]*sin(w_rest*time);
    return Et_sin;
}


adouble et_cos_plus(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest+uparam->w_rest_disp;

    adouble Et_cos = controls[CINDEX(1)]*cos(w_rest*time);
    return Et_cos;
}


adouble et_sin_plus(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest+uparam->w_rest_disp;

    adouble Et_sin = controls[CINDEX(1)]*sin(w_rest*time);
    return Et_sin;
}

adouble et_cos_minus(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest-uparam->w_rest_disp;

    adouble Et_cos = controls[CINDEX(1)]*cos(w_rest*time);
    return Et_cos;
}


adouble et_sin_minus(adouble *states, adouble *controls, adouble *parameters,
                  adouble &time, adouble *xad, int iphase, Workspace *workspace)
{
    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    double w_rest=uparam->w_rest-uparam->w_rest_disp;

    adouble Et_sin = controls[CINDEX(1)]*sin(w_rest*time);
    return Et_sin;
}

// ////////////////////////////////////////////////////////////////////////////
// ///////////////////  Define the events function ////////////////////////////
// ////////////////////////////////////////////////////////////////////////////

void events(adouble* e, adouble* initial_states, adouble* final_states,
            adouble* parameters,adouble& t0, adouble& tf, adouble* xad,
            int iphase, Workspace* workspace)
{

    User_Param *uparam = (User_Param *)workspace->problem->user_data;
    int nstates=uparam->nstates;
    double w_rest=uparam->w_rest;


    adouble u0;
    get_initial_controls(&u0, xad, iphase, workspace);

    adouble uf;
    get_final_controls(&uf, xad, iphase, workspace);

    // Compute the integral to be constrained
    adouble int_et = integrate(et, xad, iphase, workspace);

    adouble int_et_cos;
    adouble int_et_sin;

    adouble int_et_cos_plus;
    adouble int_et_sin_plus;

    adouble int_et_cos_minus;
    adouble int_et_sin_minus;


    if (w_rest != 0)
    {
        int_et_cos = integrate(et_cos, xad, iphase, workspace);
        int_et_sin = integrate(et_sin, xad, iphase, workspace);

        int_et_cos_plus = integrate(et_cos_plus, xad, iphase, workspace);
        int_et_sin_plus = integrate(et_sin_plus, xad, iphase, workspace);

        int_et_cos_minus = integrate(et_cos_minus, xad, iphase, workspace);
        int_et_sin_minus = integrate(et_sin_minus, xad, iphase, workspace);
    }
   


    for (int i=1; i<=nstates; i++)
    {
        e[ CINDEX(i) ] = initial_states[ CINDEX(i) ];
    }

    e[ CINDEX(nstates+1) ] = int_et;  //The integral of the field
    e[ CINDEX(nstates+2) ] = u0; //The control at initial time
    e[ CINDEX(nstates+3) ] = uf; //The control at the final time
    e[ CINDEX(nstates+4) ] = pow(cross_correl_2(initial_states,initial_states,nstates),1/4.); // The norm of the initial state
  

    if (w_rest != 0)
    {
        e[ CINDEX(nstates+5) ] = int_et_cos;  //Cosine coeficient of the furier transform for the w_rest component
        e[ CINDEX(nstates+6) ] = int_et_sin;  //Sin coeficient of the furier transform for the w_rest component

        e[ CINDEX(nstates+7) ] = int_et_cos_plus;  //Cosine coeficient of the furier transform for the w_rest component
        e[ CINDEX(nstates+8) ] = int_et_sin_plus;  //Sin coeficient of the furier transform for the w_rest component

        e[ CINDEX(nstates+9) ] = int_et_cos_minus;  //Cosine coeficient of the furier transform for the w_rest component
        e[ CINDEX(nstates+10) ] = int_et_sin_minus;  //Sin coeficient of the furier transform for the w_rest component
    }
    

  


}

///////////////////////////////////////////////////////////////////////////
///////////////////  Define the phase linkages function ///////////////////
///////////////////////////////////////////////////////////////////////////

void linkages( adouble* linkages, adouble* xad, Workspace* workspace)
{
  // No linkages as this is a single phase problem
}


////////////////////////////////////////////////////////////////////////////
///////////////////  Define the main routine ///////////////////////////////
////////////////////////////////////////////////////////////////////////////


int main(void)
{

////////////////////////////////////////////////////////////////////////////
///////////////////  Declare key structures ////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    Alg  algorithm;
    Sol  solution;
    Prob problem;

    // Declaring the user_param data type
    User_Param uparam;





    size_t n_states=uparam.nstates;



    // Pasing the user parameters to the problem structure
    problem.user_data = (void *) &uparam;

////////////////////////////////////////////////////////////////////////////
///////////////////  Register problem name  ////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    problem.name = "irep2";

    problem.outfilename = "out_irep2.txt";

////////////////////////////////////////////////////////////////////////////
////////////  Define problem level constants & do level 1 setup ////////////
////////////////////////////////////////////////////////////////////////////

    problem.nphases   			= 1;
    problem.nlinkages           = 0;

    psopt_level1_setup(problem);

////////////////////////////////////////////////////////////////////////////
///////////////////  Declare DMatrix objects to store results //////////////
////////////////////////////////////////////////////////////////////////////

    double tf_l = uparam.tf_l;
    double tf_u = uparam.tf_u;
    double tf= (tf_l+tf_u)/2.;
    double w_rest=uparam.w_rest;


    int nnodes = uparam.nnodes;

    double dt=tf/(nnodes-1);

    MatrixXd x0(n_states, nnodes);
    MatrixXd u0(1, nnodes);
    MatrixXd t0(1, nnodes);

    // // This are the classical period and the classsical frequency
    // double Tclass = uparam.usys.Tclass;
    // double wclass = 2 * PI / Tclass;

    // // This are the time limits for the piecewise function
    // double mid_T = (2. / 3.) * Tclass;
    // double t1 = tf / 2. - mid_T / 2.;
    // double t2 = tf / 2. + mid_T / 2.;

    // // This is to convert from i to t and viceversa
    // double i2t = tf / nnodes;
    // double t2i = nnodes / tf;





    if (uparam.init_cond == "random")
    {
        // The initial guess is random between  -sqrt(2)/2 and sqrt(2)/2 for all state variables
        x0 = sqrt(2) * MatrixXd::Random(n_states, nnodes) - (sqrt(2) / 2 * MatrixXd::Ones(n_states, nnodes));

        //u0 is random
        u0 = (uparam.u_u - uparam.u_l) * MatrixXd::Random(1, nnodes) - ( (uparam.u_u - uparam.u_l) / 2 * MatrixXd::Ones(1, nnodes));
        t0 = linspace(0.0, tf, nnodes);
    }
    else if (uparam.init_cond == "linear")
    {

        // for (size_t i = 0; i < n_states; i++)
        //     for (size_t j = 0; j < nnodes; j++)
        //         x0(i,j) = sqrt(sigmoid (j*dt, uparam.init_state(i), uparam.final_state(i), tf/2, tf/40));

        // for (size_t j = 0; j < nnodes; j++)
        //     u0(j)=sin(j*dt*(uparam.sys.E(1,1)-uparam.sys.E(0,0)))*pow(sin(j*dt * PI / tf),uparam.sin_exp);


        for (size_t i = 0; i < n_states; i++)
            x0.row(i) = linspace(uparam.init_state(i), uparam.final_state(i), nnodes);

        u0 = linspace(uparam.u_l, uparam.u_u, nnodes);
        t0 = linspace(0.0, tf, nnodes);
    }
    else
    {
        throw invalid_argument("****** ERROR *******  main: uparam.init_cond must be random or linear");
    }
    

    MatrixXd pop0 = population(x0);
    Save(pop0.transpose(),"pop0.dat");

    //Saving initial guess
    Save(x0.transpose(),"x0.dat");
    Save(u0.transpose(),"u0.dat");
    Save(t0.transpose(),"t0.dat");

/////////////////////////////////////////////////////////////////////////
/////   Define phase related information & do level 2 setup /////////////
/////////////////////////////////////////////////////////////////////////


    problem.phases(1).nstates   		= n_states;
    problem.phases(1).ncontrols 		= 1;
    problem.phases(1).nevents   		= n_states+4;
    if (w_rest != 0)
    {
        problem.phases(1).nevents   		= n_states+10;
    }
    problem.phases(1).npath     		= 0;
    problem.phases(1).nodes             = (RowVectorXi(1) << nnodes).finished();

    psopt_level2_setup(problem, algorithm);


////////////////////////////////////////////////////////////////////////////
///////////////////  Declare DMatrix objects to store results //////////////
////////////////////////////////////////////////////////////////////////////

    MatrixXd x, u, t;
    MatrixXd lambda, H;

// ////////////////////////////////////////////////////////////////////////////
// ///////////////////  Enter problem bounds information //////////////////////
// ////////////////////////////////////////////////////////////////////////////


    

    //Specifying bounds for the states
    problem.phases(1).bounds.upper.states = ones(n_states,1);
    problem.phases(1).bounds.lower.states = -1*ones(n_states,1);

    problem.phases(1).bounds.upper.controls(0) = uparam.u_u;
    problem.phases(1).bounds.lower.controls(0) = uparam.u_l;

    //Specifying bounds for the events. These impose the initial condition
    if (uparam.init_cost == 0)
    {
        for (size_t i=0; i<n_states;i++)
        {
            problem.phases(1).bounds.upper.events(i) = uparam.init_state(i);
            problem.phases(1).bounds.lower.events(i) = uparam.init_state(i);
        }
    }
    else
    {
        for (size_t i=0; i<n_states;i++)
        {
            problem.phases(1).bounds.upper.events(i) = 1;
            problem.phases(1).bounds.lower.events(i) = -1;
        }
    }
    
    
    
    
    problem.phases(1).bounds.upper.events(n_states + 0) = 0;
    problem.phases(1).bounds.lower.events(n_states + 0) = 0;

    problem.phases(1).bounds.upper.events(n_states + 1) = 0;
    problem.phases(1).bounds.lower.events(n_states + 1) = 0;

    problem.phases(1).bounds.upper.events(n_states + 2) = 0;
    problem.phases(1).bounds.lower.events(n_states + 2) = 0;

    problem.phases(1).bounds.upper.events(n_states + 3) = 1;
    problem.phases(1).bounds.lower.events(n_states + 3) = 1;


    if (w_rest != 0)
    {
        problem.phases(1).bounds.upper.events(n_states + 4) = 0;
        problem.phases(1).bounds.lower.events(n_states + 4) = 0;

        problem.phases(1).bounds.upper.events(n_states + 5) = 0;
        problem.phases(1).bounds.lower.events(n_states + 5) = 0;

        problem.phases(1).bounds.upper.events(n_states + 6) = 0;
        problem.phases(1).bounds.lower.events(n_states + 6) = 0;

        problem.phases(1).bounds.upper.events(n_states + 7) = 0;
        problem.phases(1).bounds.lower.events(n_states + 7) = 0;

        problem.phases(1).bounds.upper.events(n_states + 8) = 0;
        problem.phases(1).bounds.lower.events(n_states + 8) = 0;

        problem.phases(1).bounds.upper.events(n_states + 9) = 0;
        problem.phases(1).bounds.lower.events(n_states + 9) = 0;
    }




    //Boundary for time limits. With these initial and final time are fixed
    problem.phases(1).bounds.lower.StartTime    = 0.0;
    problem.phases(1).bounds.upper.StartTime    = 0.0;

    problem.phases(1).bounds.lower.EndTime      = tf_l;
    problem.phases(1).bounds.upper.EndTime      = tf_u;


////////////////////////////////////////////////////////////////////////////
///////////////////  Register problem functions  ///////////////////////////
////////////////////////////////////////////////////////////////////////////


    problem.integrand_cost 	= &integrand_cost;
    problem.endpoint_cost 	= &endpoint_cost;
    problem.dae 		    = &dae;
    problem.events 		    = &events;
    problem.linkages		= &linkages;


////////////////////////////////////////////////////////////////////////////
///////////////////  Define & register initial guess ///////////////////////
////////////////////////////////////////////////////////////////////////////


    problem.phases(1).guess.controls       = u0;
    problem.phases(1).guess.states         = x0;
    problem.phases(1).guess.time           = t0;

////////////////////////////////////////////////////////////////////////////
///////////////////  Enter algorithm options  //////////////////////////////
////////////////////////////////////////////////////////////////////////////


    algorithm.nlp_method                  = "IPOPT";
    algorithm.scaling                     = "automatic";
    algorithm.derivatives                 = "automatic";
    algorithm.collocation_method          = uparam.collocation_method;
    algorithm.ipopt_linear_solver         = uparam.ipopt_linear_solver;
    algorithm.nlp_iter_max                = uparam.nlp_iter_max;
    algorithm.nlp_tolerance               = uparam.nlp_tolerance;
    algorithm.nsteps_error_integration    = 1;
    algorithm.hessian                     = uparam.hessian;
    algorithm.ipopt_max_cpu_time          = uparam.ipopt_max_cpu_time;

////////////////////////////////////////////////////////////////////////////
///////////////////  Now call PSOPT to solve the problem   /////////////////
////////////////////////////////////////////////////////////////////////////

    psopt(solution, problem, algorithm);

    if (solution.error_flag) exit(0);

////////////////////////////////////////////////////////////////////////////
///////////  Extract relevant variables from solution structure   //////////
////////////////////////////////////////////////////////////////////////////

    x 		= solution.get_states_in_phase(1);
    u 		= solution.get_controls_in_phase(1);
    t 		= solution.get_time_in_phase(1);


////////////////////////////////////////////////////////////////////////////
///////////  Save solution data to files if desired ////////////////////////
////////////////////////////////////////////////////////////////////////////


    MatrixXd xs,xi,pops,popi,x_expect;    

    if (uparam.inter_schrod == "Interaction")
    {
        xi=x;
        xs=psii_2_psis(x,uparam.sys.E,t);
    }
    // else if (uparam.inter_schrod == "Schrodinger")
    // {
    //     xi=psii_2_psis(x,uparam.sys.E,-1*t);
    //     xs=x;
    // }

    popi=population(xi);
    pops=population(xs);
    // x_expect=x_expectation(xs,uparam.sys.mu);

    //Saving final solution
    Save(xi.transpose(),"xi.dat");
    Save(xs.transpose(),"xs.dat");
    Save(popi.transpose(),"popi.dat");
    Save(pops.transpose(),"pops.dat");
    // x_expect.SaveT("x_expect.dat");

    
    Save(x.transpose(),"x.dat");
    Save(u.transpose(),"u.dat");
    Save(t.transpose(),"t.dat");

    
    int inter_nnodes = 10000;

    MatrixXd inter_xi(n_states, inter_nnodes);
    MatrixXd inter_xs(n_states, inter_nnodes);
    MatrixXd inter_u(1, inter_nnodes);
    MatrixXd inter_t(1, inter_nnodes);
    MatrixXd efield(inter_nnodes, 2);
    MatrixXd inter_popi, inter_pops;


    inter_t=linspace(0.0, tf, inter_nnodes);


    resample_trajectory(inter_u, inter_t, u, t);
    resample_trajectory(inter_xi, inter_t, xi, t);
    resample_trajectory(inter_xs, inter_t, xs, t);
    efield<<inter_t.transpose(),inter_u.transpose();
    inter_popi=population(inter_xi);
    inter_pops=population(inter_xs);



    Save(inter_xi.transpose(),"xi_inter.dat");
    Save(inter_xs.transpose(),"xs_inter.dat");
    Save(inter_u.transpose(),"u_inter.dat");
    Save(inter_t.transpose(),"t_inter.dat");
    Save(efield,"efield.dat");
    Save(inter_popi.transpose(),"popi_inter.dat");




////////////////////////////////////////////////////////////////////////////
///////////  Plot some results if desired (requires gnuplot) ///////////////
////////////////////////////////////////////////////////////////////////////



    //plot(t,u,problem.name + ": control", "time (s)", "control", "u");
    // Defining where I will output the data
    ostringstream legend;
    for (size_t i=0; i<uparam.Emax; i++)
        legend << "state" << i<<" ";

    plot(inter_t, inter_popi, problem.name + ": states populations", "time (s)", "populations", legend.str().c_str(), "pdf", "popi_inter.pdf");
    plot(inter_t, inter_pops, problem.name + ": states populations", "time (s)", "populations", legend.str().c_str(), "pdf", "pops_inter.pdf");


    plot(inter_t,    inter_u,                    problem.name + ": control", "time (s)", "E(t)", "E(t)", "pdf", "u_inter.pdf");
    plot(inter_t, inter_xs(seq(0,uparam.Emax-1), all), problem.name + ": Re[schrodinger states]", "time (s)", "states", legend.str().c_str(), "pdf", "re[xs]_inter.pdf");
    plot(inter_t, inter_xs(seq(uparam.Emax,last), all), problem.name + ": Im[ schrodinger states]", "time (s)", "states", legend.str().c_str(), "pdf", "im[xs]_inter.pdf");
    plot(inter_t, inter_xi(seq(0,uparam.Emax-1), all), problem.name + ": Re[interaction states]", "time (s)", "states", legend.str().c_str(), "pdf", "re[xi]_inter.pdf");
    plot(inter_t, inter_xi(seq(uparam.Emax,last), all), problem.name + ": Im[interaction states]", "time (s)", "states", legend.str().c_str(), "pdf", "im[xi]_inter.pdf");



    plot(t, popi, problem.name + ": states populations", "time (s)", "populations", legend.str().c_str(), "pdf", "popi.pdf");
    plot(t, pops, problem.name + ": states populations", "time (s)", "populations", legend.str().c_str(), "pdf", "pops.pdf");


    plot(t, u,                    problem.name + ": control", "time (s)", "E(t)", "E(t)", "pdf", "u.pdf");
    plot(t, xs(seq(0,uparam.Emax-1), all), problem.name + ": Re[schrodinger states]", "time (s)", "states", legend.str().c_str(), "pdf", "re[xs].pdf");
    plot(t, xs(seq(uparam.Emax,last), all), problem.name + ": Im[ schrodinger states]", "time (s)", "states", legend.str().c_str(), "pdf", "im[xs].pdf");
    plot(t, xi(seq(0,uparam.Emax-1), all), problem.name + ": Re[interaction states]", "time (s)", "states", legend.str().c_str(), "pdf", "re[xi].pdf");
    plot(t, xi(seq(uparam.Emax,last), all), problem.name + ": Im[interaction states]", "time (s)", "states", legend.str().c_str(), "pdf", "im[xi].pdf");




//     return 0;




}

////////////////////////////////////////////////////////////////////////////
///////////////////////      END OF FILE     ///////////////////////////////
////////////////////////////////////////////////////////////////////////////


