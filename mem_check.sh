#! /bin/bash

valgrind --leak-check=yes \
         --log-file=valgrind-out.txt \
         ./irep2 < mu0_41.96970127258238_e0_0.008712861712035003_yf_1_umax_500_end_cost_1_random_Emax1_30_Emax2_10_T_150_nnodes_300_0.000.inp
