#! /bin/bash

source ~/.profile

out_file=out

T1s=$(date +%s)

    
    eigenvec_file="/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_10_ny_20/eigenvec.txt"
    emax=4
    vec=row
    
python3 $PYTHON_SCRIPTS/ho_state_selector.py cp_rest_states_index.txt $eigenvec_file $emax $vec
python3 $PYTHON_SCRIPTS/ho_state_selector.py cp_y0_index.txt $eigenvec_file $emax $vec
python3 $PYTHON_SCRIPTS/ho_state_selector.py cp_yf_index.txt $eigenvec_file $emax $vec


./irep < inp > $out_file 2>&1

T2s=$(date +%s)
RUN_TIMEsec=$(($T2s-$T1s))
RUN_TIMEmin=$(echo "scale=2;$RUN_TIMEsec/60" | bc)
RUN_TIMEh=$(echo "scale=2;$RUN_TIMEsec/3600" | bc)
RUN_TIMEd=$(echo "scale=2;$RUN_TIMEsec/86400" | bc)




echo >> $out_file
echo >> $out_file
echo >> $out_file
echo >> $out_file
echo "Runtime is: ${RUN_TIMEsec} sec" >> $out_file
echo "Runtime is: ${RUN_TIMEmin} min" >> $out_file
echo "Runtime is: ${RUN_TIMEh} hours" >> $out_file
echo "Runtime is: ${RUN_TIMEd} days" >> $out_file


python3 $PYTHON_SCRIPTS/even_space.py t.dat u.dat efield.txt 1000
